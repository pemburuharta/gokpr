package com.app.dots.gokpr.presenter.bank;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.app.dots.gokpr.Helper.SharedPref;
import com.app.dots.gokpr.activity.AddBankActivity;
import com.app.dots.gokpr.adapter.AdapterDaftarBank;
import com.app.dots.gokpr.adapter.AdapterPromo;
import com.app.dots.gokpr.model.DataUserModel;
import com.app.dots.gokpr.model.ResponseModel;
import com.app.dots.gokpr.retrofit.ApiConfig;
import com.app.dots.gokpr.retrofit.ApiService;

import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class BankPresenter {

    private Context context;
    private ArrayList<DataUserModel> dataUserModels = new ArrayList<>();
    private AdapterDaftarBank adapter;
    private com.app.dots.gokpr.Helper.SharedPref pref;

    public BankPresenter(Context context) {
        this.context = context;
        pref = new SharedPref(context);
    }

    public void getDaftarBank(){
        com.app.dots.gokpr.Helper.Loading.onLoading(context);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getDaftarBank()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseModel responseModel) {
                        dataUserModels = responseModel.databank;
                    }

                    @Override
                    public void onError(Throwable e) {
                        com.app.dots.gokpr.Helper.Loading.dismis();
                        com.app.dots.gokpr.Helper.Loading.errorBank(context,"Error",""+e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        com.app.dots.gokpr.Helper.Loading.dismis();
                        ((AddBankActivity)context).setBank(dataUserModels);
                    }
                });
    }
    public void getRekening(final RecyclerView recyclerView){
        com.app.dots.gokpr.Helper.Loading.onLoading(context);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getRekening(pref.getIDUSER())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseModel responseModel) {
                        dataUserModels = responseModel.datarekening;
                    }

                    @Override
                    public void onError(Throwable e) {
                        com.app.dots.gokpr.Helper.Loading.dismis();
                        com.app.dots.gokpr.Helper.Loading.errorRekening(context,"Error",""+e.getMessage(),recyclerView);
                    }

                    @Override
                    public void onComplete() {
                        com.app.dots.gokpr.Helper.Loading.dismis();
                        setDataRekening(dataUserModels,recyclerView);
                    }
                });
    }

    public void setDataRekening(ArrayList<DataUserModel> dataBank, RecyclerView recyclerView){

        recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
        adapter = new AdapterDaftarBank(context,dataBank);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public void insertBank(final String id, final String nama, final String rekening){
        com.app.dots.gokpr.Helper.Loading.onLoading(context);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.addRekening(pref.getIDUSER(),
                id,
                nama,
                rekening)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseModel responseModel) {
                        if (responseModel.error == false){
                            Toast.makeText(context, ""+responseModel.message, Toast.LENGTH_SHORT).show();
                            ((AddBankActivity)context).clear();
                        }else {
                            Toast.makeText(context, ""+responseModel.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        com.app.dots.gokpr.Helper.Loading.dismis();
                        com.app.dots.gokpr.Helper.Loading.errorAddBank(context,"Error",""+e.getMessage(),id,nama,rekening);
                    }

                    @Override
                    public void onComplete() {
                        com.app.dots.gokpr.Helper.Loading.dismis();
                    }
                });
    }
}
