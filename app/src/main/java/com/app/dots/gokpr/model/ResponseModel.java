package com.app.dots.gokpr.model;

import java.util.ArrayList;

public class ResponseModel {
    public boolean error;
    public String message;
    public DataUserModel data;
    public ArrayList<DataUserModel> dataPromo = new ArrayList<>();
    public ArrayList<DataUserModel> databank = new ArrayList<>();
    public ArrayList<DataUserModel> datarekening = new ArrayList<>();
}
