package com.app.dots.gokpr.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.app.dots.gokpr.fragment.HistoryPinjamanFragment;
import com.app.dots.gokpr.R;

import java.util.ArrayList;
import java.util.List;

public class HistoryPenarikanActivity extends AppCompatActivity {

    private ImageView penarikanIvBack;
    private TabLayout penarikanTablayout;
    private ViewPager penarikanViewpager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_penarikan);
        getSupportActionBar().hide();
        initView();

        setupViewPager(penarikanViewpager);
        penarikanTablayout.setupWithViewPager(penarikanViewpager);
        
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new HistoryPinjamanFragment(),"Menunggu");
        adapter.addFragment(new HistoryPinjamanFragment(),"Diproses");
        adapter.addFragment(new HistoryPinjamanFragment(),"Ditolak");
        adapter.addFragment(new HistoryPinjamanFragment(),"Di ACC");
        viewPager.setAdapter(adapter);
    }

    private void initView() {
        penarikanIvBack = findViewById(R.id.penarikan_iv_back);
        penarikanTablayout = findViewById(R.id.penarikan_tablayout);
        penarikanViewpager = findViewById(R.id.penarikan_viewpager);
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
        }

        @Override
        public Fragment getItem(int i) {
            return mFragmentList.get(i);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String s) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(s);
        }

    }

}
