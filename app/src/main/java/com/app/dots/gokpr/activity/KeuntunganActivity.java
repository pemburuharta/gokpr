package com.app.dots.gokpr.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.dots.gokpr.R;
import com.app.dots.gokpr.model.DataUserModel;
import com.app.dots.gokpr.presenter.profil.ProfilPresenter;

public class KeuntunganActivity extends AppCompatActivity {

    private ImageView keuntunganIvBack;
    private TextView keuntunganTvPoin;
    private CardView keuntunganCvBank;
    private CardView keuntunganCvWithdraw;
    private ProfilPresenter profilPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keuntungan);
        getSupportActionBar().hide();
        initView();

        start();
    }

    private void start(){
        profilPresenter = new ProfilPresenter(KeuntunganActivity.this);
        profilPresenter.getPoin();
        mainButton();
    }

    public void setPoin(String poin){
        keuntunganTvPoin.setText(new ai.agusibrhim.design.topedc.Helper.HelperClass().convertRupiah(Integer.parseInt(poin)));
    }

    private void mainButton(){
        keuntunganIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        keuntunganCvBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ListBankActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        keuntunganCvWithdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), WithdrawActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

    private void initView() {
        keuntunganIvBack = findViewById(R.id.keuntungan_iv_back);
        keuntunganTvPoin = findViewById(R.id.keuntungan_tv_poin);
        keuntunganCvBank = findViewById(R.id.keuntungan_cv_bank);
        keuntunganCvWithdraw = findViewById(R.id.keuntungan_cv_withdraw);
    }
}
