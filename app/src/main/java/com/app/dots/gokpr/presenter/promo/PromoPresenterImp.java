package com.app.dots.gokpr.presenter.promo;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.widget.TextView;
import android.widget.Toast;

import com.app.dots.gokpr.Helper.SharedPref;
import com.app.dots.gokpr.R;
import com.app.dots.gokpr.adapter.AdapterPromo;
import com.app.dots.gokpr.model.DataUserModel;
import com.app.dots.gokpr.model.ModelPromo;
import com.app.dots.gokpr.model.ResponseModel;
import com.app.dots.gokpr.retrofit.ApiConfig;
import com.app.dots.gokpr.retrofit.ApiService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class PromoPresenterImp implements PromoPresenter {

    protected AdapterPromo adapter;
    protected List<ModelPromo> models;
    private ArrayList<DataUserModel> listPromo = new ArrayList<>();
    private Context context;
    private Calendar calendar;
    private com.app.dots.gokpr.Helper.SharedPref pref;

    public PromoPresenterImp(Context context) {
        this.context = context;
        pref = new SharedPref(context);
    }

    @Override
    public void getPromo(final ViewPager viewPager) {
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getPromo()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseModel responseModel) {
                        listPromo = responseModel.dataPromo;
                    }

                    @Override
                    public void onError(Throwable e) {
                        com.app.dots.gokpr.Helper.Loading.reloadPromo(context,"Error",""+e.getMessage(),viewPager);
                    }

                    @Override
                    public void onComplete() {
                        adapter = new AdapterPromo(listPromo, context);
                        viewPager.setAdapter(adapter);
                        viewPager.setPadding(30, 0, 30, 0);
                    }
                });
    }

    @Override
    public void setWaktu(TextView tvSelamat, TextView tvNama) {
        calendar = Calendar.getInstance();
        int timeOfDay = calendar.get(Calendar.HOUR_OF_DAY);

        if (timeOfDay >= 0 && timeOfDay < 10) {
            tvSelamat.setText("Selamat Pagi");
        } else if (timeOfDay >= 10 && timeOfDay < 16) {
            tvSelamat.setText("Selamat Siang");
        } else if (timeOfDay >= 16 && timeOfDay < 18) {
            tvSelamat.setText("Selamat Sore");
        } else if (timeOfDay >= 18 && timeOfDay < 24) {
            tvSelamat.setText("Selamat Malam");
        }
        tvNama.setText(pref.getNAMA());
    }
}
