package com.app.dots.gokpr.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.app.dots.gokpr.R;
import com.app.dots.gokpr.fragment.HistoryPenarikanFragment;
import com.app.dots.gokpr.fragment.HistoryPinjamanFragment;
import com.app.dots.gokpr.fragment.HomeFragment;
import com.app.dots.gokpr.fragment.ProfileFragment;

public class MainHomeActivity extends AppCompatActivity {
    private Menu menu;
    private MenuItem menuItem;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_home);
        getSupportActionBar().hide();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame, new HomeFragment())
                .commit();

        buttomNav();
    }

    private void buttomNav() {
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        menu = bottomNavigationView.getMenu();
        menuItem = menu.getItem(0);
        menuItem.setChecked(true);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        menuItem = menu.getItem(0);
                        menuItem.setChecked(true);
                        callFragment(new HomeFragment());
                        break;
                    case R.id.navigation_dashboard:
                        menuItem = menu.getItem(1);
                        menuItem.setChecked(true);
                        callFragment(new HistoryPinjamanFragment());
                        break;

                    case R.id.navigation_notifications:
                        menuItem = menu.getItem(2);
                        menuItem.setChecked(true);
                        callFragment(new HistoryPenarikanFragment());
                        break;

                    case R.id.navigation_user:
                        menuItem = menu.getItem(3);
                        menuItem.setChecked(true);
                        callFragment(new ProfileFragment());
                        break;
                }
                return false;
            }
        });
    }

    private void callFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame, fragment)
                .commit();
    }
}
