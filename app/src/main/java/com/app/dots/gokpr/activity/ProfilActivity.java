package com.app.dots.gokpr.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.app.dots.gokpr.Helper.Config;
import com.app.dots.gokpr.Helper.SharedPref;
import com.app.dots.gokpr.MainActivity;
import com.app.dots.gokpr.R;
import com.app.dots.gokpr.presenter.login.LoginPresenterImp;
import com.app.dots.gokpr.presenter.profil.ProfilPresenter;
import com.app.dots.gokpr.presenter.promo.PromoPresenterImp;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import io.reactivex.annotations.NonNull;

public class ProfilActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private TextView tvKeluar;
    private LoginPresenterImp loginPresenterImp;
    private ProfilPresenter profilPresenter;
    private SharedPref pref;
    private GoogleApiClient googleApiClient;
    private TextView tvUsername;
    private TextView tvReveral;
    private TextView tvNama;
    private TextView tvEmail;
    private TextView tvNoTlp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);
        getSupportActionBar().hide();
        initView();

        start();
    }

    private void start() {
        pref = new SharedPref(ProfilActivity.this);
        loginPresenterImp = new LoginPresenterImp(ProfilActivity.this);
        profilPresenter = new ProfilPresenter(ProfilActivity.this);
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions).build();

//        profilPresenter.getProfil();

        mainButton();
    }

    public void setData(String username,String reveral,String nama,String email,String tlp) {
        tvUsername.setText(username);
        tvReveral.setText("Reveral : "+ reveral);
        tvNama.setText(nama);
        tvEmail.setText(email);
        tvNoTlp.setText(tlp);
    }

    private void mainButton() {
        tvKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pref.getTIPELOGIN().equals(Config.GMAIL)) {
                    Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                            pref.clearAll();

                            pref.savePrefBoolean(SharedPref.SUDAH_LOGIN, false);

                            startActivity(new Intent(getApplicationContext(), LoginActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        }
                    });

                } else {
                    loginPresenterImp.Logout();
                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void initView() {
        tvKeluar = findViewById(R.id.tv_keluar);
        tvUsername = findViewById(R.id.tv_username);
        tvReveral = findViewById(R.id.tv_reveral);
        tvNama = findViewById(R.id.tv_nama);
        tvEmail = findViewById(R.id.tv_email);
        tvNoTlp = findViewById(R.id.tv_no_tlp);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
