package com.app.dots.gokpr.presenter.register;

import android.content.Context;
import android.content.Intent;
import android.widget.EditText;
import android.widget.Toast;

import com.app.dots.gokpr.Helper.Config;
import com.app.dots.gokpr.Helper.SharedPref;
import com.app.dots.gokpr.MainActivity;
import com.app.dots.gokpr.activity.LoginActivity;
import com.app.dots.gokpr.activity.RegisterActivity;
import com.app.dots.gokpr.Helper.Loading;
import com.app.dots.gokpr.model.ResponseModel;
import com.app.dots.gokpr.retrofit.ApiConfig;
import com.app.dots.gokpr.retrofit.ApiService;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class RegisterPresenterImp {

    private Context context;
    private com.app.dots.gokpr.Helper.SharedPref pref;

    public RegisterPresenterImp(Context context) {
        this.context = context;
        pref = new SharedPref(context);
    }

    public void register(EditText nama,EditText username, EditText email, EditText no, EditText Pass,EditText edtReferal){
        Loading.onLoading(context,"Proses ...");
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.register(nama.getText().toString(),username.getText().toString(),
                email.getText().toString(),
                Pass.getText().toString(),
                no.getText().toString(),
                edtReferal.getText().toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseModel responseModel) {
                        if (responseModel.error == true){
                            Toast.makeText(context, ""+responseModel.message, Toast.LENGTH_SHORT).show();
                        }else {
                            if (pref.getTIPELOGIN().equals(Config.GMAIL)){
                                pref.savePrefBoolean(SharedPref.SUDAH_LOGIN,true);
                                pref.savePrefString(SharedPref.TIPELOGIN, Config.GMAIL);
                                Intent intent = new Intent(context, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(intent);
                                ((RegisterActivity)context).finish();

                            }else {
                                Toast.makeText(context, ""+responseModel.message, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(intent);
                                ((RegisterActivity)context).finish();
                            }

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Loading.dialogDismiss();
                        Toast.makeText(context, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {
                        Loading.dialogDismiss();
                    }
                });
    }
}
