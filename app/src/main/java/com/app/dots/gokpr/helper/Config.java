package com.app.dots.gokpr.Helper;

public final class Config {
    public static final String BASE_URL ="https://apigokpr.ods.web.id/";
    public static final String URL_REGISTER ="API/register";
    public static final String URL_LOGIN ="API/login";
    public static final String URL_CEKGMAIL ="API/api_authemail";
    public static final String URLPROMO ="API/Getpromo";
    public static final String URLPROFIL ="API/get_profil";
    public static final String URLPOIN ="API/cek_poin";
    public static final String URLDAFTARBANK ="API/daftar_bank";
    public static final String URLDAFTARREKENING ="API/daftar_rekening_customer";
    public static final String URLADDREK ="API/tambah_rekening";
    public static final String URLIMAGE ="https://apigokpr.ods.web.id/assets/images/";
    public static final String URLIMAGE2 ="https://apigokpr.ods.web.id/assets/thumb/";
    public static final String MANUAL ="manual";
    public static final String GMAIL ="gmail";
}
