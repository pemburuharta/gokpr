package com.app.dots.gokpr.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.dots.gokpr.Helper.Config;
import com.app.dots.gokpr.Helper.SharedPref;
import com.app.dots.gokpr.activity.DetailPromoActivity;
import com.app.dots.gokpr.model.DataUserModel;
import com.app.dots.gokpr.model.ModelPromo;
import com.app.dots.gokpr.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class AdapterPromo extends PagerAdapter {

    private ArrayList<DataUserModel> models;
    private LayoutInflater layoutInflater;
    private Context context;
    private com.app.dots.gokpr.Helper.SharedPref pref;

    public AdapterPromo(ArrayList<DataUserModel> models, Context context) {
        this.models = models;
        this.context = context;
        pref = new SharedPref(context);
    }

    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.item_promo, container, false);

        ImageView imageView;
        CardView cardView;

        imageView = view.findViewById(R.id.promo_iv_thumb);
        cardView = view.findViewById(R.id.promo_cv_main);

        Picasso.with(context)
                .load(Config.URLIMAGE2 + models.get(position).gambar)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.no_image_found)
                .into(imageView);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pref.setData(models.get(position));

                Intent intent = new Intent(context, DetailPromoActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        container.addView(view, 0);

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }
}
