package com.app.dots.gokpr.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.LinearLayout;

import com.app.dots.gokpr.Helper.SharedPref;
import com.app.dots.gokpr.MainActivity;
import com.app.dots.gokpr.R;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.github.florent37.viewanimator.ViewAnimator;

public class SplashScreen2Activity extends AppCompatActivity {

    private LinearLayout splash2DivLogo;
    private LinearLayout splash2DivButton;
    private CardView splash2CvMasuk;
    private CardView splash2CvDaftar;
    private com.app.dots.gokpr.Helper.SharedPref pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen2);
        getSupportActionBar().hide();
        Animatoo.animateSlideLeft(this);
        initView();

        Start();
    }

    private void Start(){
        pref = new SharedPref(this);

        if (pref.getSudahLogin()){
            startActivity(new Intent(getApplicationContext(), MainHomeActivity.class));
            finish();
        }else {
            ViewAnimator
                    .animate(splash2CvDaftar)
                    .translationX(100, 0)
                    .duration(600)
                    .andAnimate(splash2CvMasuk)
                    .translationX(-100, 0)
                    .duration(600)
                    .andAnimate(splash2DivLogo)
                    .translationY(-1000, 0)
                    .duration(1000)
                    .start();
        }

        mainButton();
    }

    private void mainButton(){
        splash2CvMasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        });

        splash2CvDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
            }
        });
    }

    private void initView() {
        splash2DivLogo = findViewById(R.id.splash2_div_logo);
        splash2DivButton = findViewById(R.id.splash2_div_button);
        splash2CvMasuk = findViewById(R.id.splash2_cv_masuk);
        splash2CvDaftar = findViewById(R.id.splash2_cv_daftar);
    }
}
