package com.app.dots.gokpr.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.dots.gokpr.R;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;

public class MasukanPinjamanActivity extends AppCompatActivity {

    private ImageView pinjamanIvBack;
    private CheckBox pengajuanCbKpr;
    private CheckBox pengajuanCbKpa;
    private CheckBox pengajuanCbMultiguna;
    private CheckBox pengajuanCbTakeover;
    private LinearLayout pengajuanDivTakeover;
    private LinearLayout pengajuanDivUploadtakeover;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_masukan_pinjaman);
        getSupportActionBar().hide();
        initView();
        Animatoo.animateSlideLeft(this);

        mainButton();
    }

    private void mainButton(){
        pengajuanCbTakeover.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (pengajuanCbTakeover.isChecked()){
                    pengajuanDivTakeover.setVisibility(View.VISIBLE);
                    pengajuanDivUploadtakeover.setVisibility(View.VISIBLE);
                }else {
                    pengajuanDivTakeover.setVisibility(View.GONE);
                    pengajuanDivUploadtakeover.setVisibility(View.GONE);
                }
            }
        });

        pinjamanIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Animatoo.animateSlideRight(this);
    }

    private void initView() {
        pinjamanIvBack = findViewById(R.id.pinjaman_iv_back);
        pengajuanCbKpr = findViewById(R.id.pengajuan_cb_kpr);
        pengajuanCbKpa = findViewById(R.id.pengajuan_cb_kpa);
        pengajuanCbMultiguna = findViewById(R.id.pengajuan_cb_multiguna);
        pengajuanCbTakeover = findViewById(R.id.pengajuan_cb_takeover);
        pengajuanDivTakeover = findViewById(R.id.pengajuan_div_takeover);
        pengajuanDivUploadtakeover = findViewById(R.id.pengajuan_div_uploadtakeover);
    }
}
