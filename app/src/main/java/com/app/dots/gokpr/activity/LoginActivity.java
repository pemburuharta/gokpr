package com.app.dots.gokpr.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.dots.gokpr.Helper.Config;
import com.app.dots.gokpr.Helper.SharedPref;
import com.app.dots.gokpr.MainActivity;
import com.app.dots.gokpr.R;
import com.app.dots.gokpr.model.ResponseModel;
import com.app.dots.gokpr.presenter.login.LoginPresenterImp;
import com.app.dots.gokpr.retrofit.ApiConfig;
import com.app.dots.gokpr.retrofit.ApiService;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private EditText loginEdtUsername;
    private EditText loginEdtPassword;
    private CardView loginCvMasuk;
    private TextView loginTvDaftar;
    private LoginPresenterImp loginPresenterImp;
    private SharedPref pref;
    private SignInButton btnGmail;
    GoogleApiClient googleApiClient;
    private static final int REQ_CODE = 9001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        Animatoo.animateSlideUp(this);
        initView();

        start();

    }

    private void start() {
        pref = new SharedPref(LoginActivity.this);
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions).build();
        loginPresenterImp = new LoginPresenterImp(LoginActivity.this);

        if (pref.getSudahLogin()) {
            startActivity(new Intent(getApplicationContext(), MainHomeActivity.class));
            finish();
        } else {

        }

        mainButton();
    }

    private void mainButton() {
        loginCvMasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validasi()) {
                    return;
                }
                loginPresenterImp.Login(loginEdtUsername, loginEdtPassword);
            }
        });

        loginTvDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        btnGmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginGmail();
            }
        });
    }

    private void loginGmail() {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(intent, REQ_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        callbackManager.onActivityResult(requestCode,resultCode,data);
        if (requestCode == REQ_CODE) {
            GoogleSignInResult googleSignInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleRequest(googleSignInResult);
        }
    }

    private void handleRequest(GoogleSignInResult result) {
        if (result.isSuccess()) {
//            Toast.makeText(this, ""+result.getStatus(), Toast.LENGTH_SHORT).show();
            GoogleSignInAccount account = result.getSignInAccount();
            String nama = account.getDisplayName();
            String email = account.getEmail();
            loginPresenterImp.cekEmail(email);
        } else {
            loginPresenterImp.updateUI(false);
        }
    }

    private boolean validasi() {
        if (loginEdtUsername.getText().toString().isEmpty()) {
            loginEdtUsername.requestFocus();
            loginEdtUsername.setError("Harus diisi");
            return false;
        }

        if (loginEdtPassword.getText().toString().isEmpty()) {
            loginEdtPassword.requestFocus();
            loginEdtPassword.setError("Harus diisi");
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Animatoo.animateSlideRight(this);
    }

    private void initView() {
        loginEdtUsername = findViewById(R.id.login_edt_username);
        loginEdtPassword = findViewById(R.id.login_edt_password);
        loginCvMasuk = findViewById(R.id.login_cv_masuk);
        loginTvDaftar = findViewById(R.id.login_tv_daftar);
        btnGmail = findViewById(R.id.btn_gmail);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
