package com.app.dots.gokpr.model;

public class DataUserModel {
    public String username;
    public String nama_lengkap;
    public String email;
    public String id_user;
    public String nama_user;
    public String telp;
    public String referal;

    //promo
    public String promo;
    public String id_promo;
    public String judul;
    public String isi;
    public String diskon;
    public String gambar;
    public String staus;

    //point
    public String poin;

    //bank
    public String nomor;
    public String id_bank;
    public String nama_bank;

    //rekening
    public String nama;
    public String rekening;
    public String nama_customers;
    public String alamat;
}
