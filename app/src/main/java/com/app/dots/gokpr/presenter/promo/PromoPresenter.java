package com.app.dots.gokpr.presenter.promo;

import android.support.v4.view.ViewPager;
import android.widget.TextView;

public interface PromoPresenter {
    void getPromo(ViewPager viewPager);
    void setWaktu(TextView tvSelamat,TextView tvNama);
}
