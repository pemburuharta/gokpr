package com.app.dots.gokpr.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.dots.gokpr.R;
import com.app.dots.gokpr.presenter.bank.BankPresenter;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;

public class WithdrawActivity extends AppCompatActivity {

    private ImageView withdrawIvBack;
    private TextView withdrawTvPoin;
    private EditText withdrawEdtJumlahwithdraw;
    private CardView withdrawCvSave;
    private RecyclerView rv;
    private BankPresenter bankPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw);
        getSupportActionBar().hide();
        Animatoo.animateSlideUp(this);
        initView();

        start();
    }

    private void start(){
        bankPresenter = new BankPresenter(WithdrawActivity.this);
        /*** Set Statusbar Jadi Hitam ***/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = getWindow().getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        /*** Tutup Statusbar Jadi Hitam ***/
        bankPresenter.getRekening(rv);

        mainButton();
    }

    private void mainButton(){
        withdrawIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        withdrawCvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Animatoo.animateSlideRight(this);
    }

    private void initView() {
        withdrawIvBack = findViewById(R.id.withdraw_iv_back);
        withdrawTvPoin = findViewById(R.id.withdraw_tv_poin);
        withdrawEdtJumlahwithdraw = findViewById(R.id.withdraw_edt_jumlahwithdraw);
        withdrawCvSave = findViewById(R.id.withdraw_cv_save);
        rv = findViewById(R.id.rv);
    }
}
