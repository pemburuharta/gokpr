package com.app.dots.gokpr.presenter.login;

import android.content.Context;
import android.content.Intent;
import android.widget.EditText;
import android.widget.Toast;

import com.app.dots.gokpr.Helper.Config;
import com.app.dots.gokpr.MainActivity;
import com.app.dots.gokpr.activity.LoginActivity;
import com.app.dots.gokpr.activity.MainHomeActivity;
import com.app.dots.gokpr.activity.ProfilActivity;
import com.app.dots.gokpr.activity.RegisterActivity;
import com.app.dots.gokpr.Helper.Loading;
import com.app.dots.gokpr.Helper.SharedPref;
import com.app.dots.gokpr.model.ResponseModel;
import com.app.dots.gokpr.retrofit.ApiConfig;
import com.app.dots.gokpr.retrofit.ApiService;

import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LoginPresenterImp {
    private Context context;
    private SharedPref pref;

    public LoginPresenterImp(Context context) {
        this.context = context;
        pref = new SharedPref(context);
    }

    public void Login(EditText edtUsername, EditText edtPassword) {
        Loading.onLoading(context,"Proses ...");
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.login(edtUsername.getText().toString(),
                edtPassword.getText().toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseModel responseModel) {
                        Loading.dialogDismiss();

                        if (responseModel.error == true){
                            Toast.makeText(context, ""+responseModel.message, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(context, ""+responseModel.message, Toast.LENGTH_SHORT).show();
                            pref.savePrefBoolean(SharedPref.SUDAH_LOGIN,true);
                            pref.savePrefString(SharedPref.TIPELOGIN, Config.MANUAL);
                            pref.savePrefString(SharedPref.IDUSER,responseModel.data.id_user);
                            pref.savePrefString(SharedPref.NAMA,responseModel.data.nama_lengkap);

                            Intent intent = new Intent(context, MainHomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                            ((LoginActivity)context).finish();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Loading.dialogDismiss();
                        Toast.makeText(context, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {
                        Loading.dialogDismiss();
                    }
                });
    }

    public void Logout() {
        pref.savePrefBoolean(SharedPref.SUDAH_LOGIN,false);
        pref.clearAll();

        Intent intent = new Intent(context, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        ((ProfilActivity)context).finish();
    }

    public void cekEmail(final String email){
        com.app.dots.gokpr.Helper.Loading.onLoading(context,"Proses ...");
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.loginGmail(email)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseModel responseModel) {
                        com.app.dots.gokpr.Helper.Loading.dialogDismiss();
                        if (responseModel.error == true){

                            pref.savePrefString(SharedPref.GMAILLOGIN,email);
                            pref.savePrefString(SharedPref.TIPELOGIN, Config.GMAIL);

                            Intent intent = new Intent(context, RegisterActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                            ((LoginActivity)context).finish();
                        }else {
                            Toast.makeText(context, ""+responseModel.message, Toast.LENGTH_SHORT).show();
                            pref.savePrefBoolean(SharedPref.SUDAH_LOGIN,true);
                            pref.savePrefString(SharedPref.TIPELOGIN, Config.GMAIL);
                            pref.savePrefString(SharedPref.IDUSER,responseModel.data.id_user);
                            pref.savePrefString(SharedPref.NAMA,responseModel.data.nama_lengkap);

                            Intent intent = new Intent(context, MainHomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                            ((LoginActivity)context).finish();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        com.app.dots.gokpr.Helper.Loading.dialogDismiss();
                        Toast.makeText(context, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {
                        com.app.dots.gokpr.Helper.Loading.dialogDismiss();
                    }
                });
    }

    public void updateUI(boolean isLogin) {
        if (isLogin) {
            Toast.makeText(context, "Login gmail berhasil", Toast.LENGTH_SHORT).show();
            pref.savePrefBoolean(SharedPref.SUDAH_LOGIN, true);
            context.startActivity(new Intent(context, MainActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            ((LoginActivity)context).finish();
        }
    }
}
