package com.app.dots.gokpr.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.app.dots.gokpr.BuildConfig;
import com.app.dots.gokpr.R;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;

public class SplashScreenActivity extends AppCompatActivity {

    private TextView tvVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        getSupportActionBar().hide();
        initView();
        tvVersion.setText("Version B 1.1");

        Animatoo.animateFade(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(), SplashScreen2Activity.class));
                finish();
            }
        }, 2000);

    }

    private void initView() {
        tvVersion = findViewById(R.id.tv_version);
    }
}
