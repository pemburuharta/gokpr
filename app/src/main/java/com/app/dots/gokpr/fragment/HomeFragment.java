package com.app.dots.gokpr.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.app.dots.gokpr.MainActivity;
import com.app.dots.gokpr.R;
import com.app.dots.gokpr.activity.HistoryPenarikanActivity;
import com.app.dots.gokpr.activity.HistoryPinjamanActivity;
import com.app.dots.gokpr.activity.KeuntunganActivity;
import com.app.dots.gokpr.activity.MasukanPinjamanActivity;
import com.app.dots.gokpr.activity.ProfilActivity;
import com.app.dots.gokpr.presenter.promo.PromoPresenterImp;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    private LinearLayout lyUtama;
    private ViewPager homeViewPager;
    private CardView homeCvMasukanpinjaman;
    private CardView homeCvKeuntungan;
    private PromoPresenterImp promoPresenterImp;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        initView(view);

        start();

        return view;
    }

    private void start(){
        promoPresenterImp = new PromoPresenterImp(getActivity());
        promoPresenterImp.getPromo(homeViewPager);

        mainButton();
    }

    private void mainButton() {

        homeCvMasukanpinjaman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MasukanPinjamanActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        homeCvKeuntungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), KeuntunganActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

    }

    private void initView(View view) {
        lyUtama = view.findViewById(R.id.ly_utama);
        homeViewPager = view.findViewById(R.id.home_viewPager);
        homeCvMasukanpinjaman = view.findViewById(R.id.home_cv_masukanpinjaman);
        homeCvKeuntungan = view.findViewById(R.id.home_cv_keuntungan);
    }
}
