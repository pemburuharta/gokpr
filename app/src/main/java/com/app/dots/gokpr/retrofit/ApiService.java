package com.app.dots.gokpr.retrofit;

import com.app.dots.gokpr.Helper.Config;
import com.app.dots.gokpr.model.ResponseModel;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiService {

    @FormUrlEncoded
    @POST(Config.URL_REGISTER)
    Observable<ResponseModel> register(
            @Field("nama_lengkap") String nama,
            @Field("username") String username,
            @Field("email") String email,
            @Field("password") String password,
            @Field("telp") String tlp,
            @Field("referal") String referal);

    @FormUrlEncoded
    @POST(Config.URL_LOGIN)
    Observable<ResponseModel> login(
            @Field("username") String username,
            @Field("password") String password);

    @FormUrlEncoded
    @POST(Config.URL_CEKGMAIL)
    Observable<ResponseModel> loginGmail(
            @Field("email") String username);

    @FormUrlEncoded
    @POST(Config.URLPROFIL)
    Observable<ResponseModel> getProfil(
            @Field("id_user") String id);

    @FormUrlEncoded
    @POST(Config.URLPOIN)
    Observable<ResponseModel> getPoint(
            @Field("id_user") String id);

    @FormUrlEncoded
    @POST(Config.URLDAFTARREKENING)
    Observable<ResponseModel> getRekening(
            @Field("id_user") String id);

    @FormUrlEncoded
    @POST(Config.URLADDREK)
    Observable<ResponseModel> addRekening(
            @Field("id_user") String id,
            @Field("id_bank") String idb,
            @Field("rekening") String rek,
            @Field("nama") String nama);

    @GET(Config.URLPROMO)
    Observable<ResponseModel> getPromo();

    @GET(Config.URLDAFTARBANK)
    Observable<ResponseModel> getDaftarBank();
}
