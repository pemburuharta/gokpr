package com.app.dots.gokpr.model;

public class ModelPromo {

    private int image;

    public ModelPromo(int image) {
        this.image = image;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

}
