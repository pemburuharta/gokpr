package com.app.dots.gokpr.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.app.dots.gokpr.R;
import com.app.dots.gokpr.presenter.bank.BankPresenter;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;

public class ListBankActivity extends AppCompatActivity {

    private ImageView listbankIvBack;
    private FloatingActionButton listbankFabAdd;
    private BankPresenter bankPresenter;
    private RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_bank);
        Animatoo.animateSlideLeft(this);
        getSupportActionBar().hide();
        initView();

        start();
    }

    private void start() {
        bankPresenter = new BankPresenter(ListBankActivity.this);
        bankPresenter.getRekening(rv);

        mainButton();
    }

    private void mainButton() {
        listbankIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        listbankFabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AddBankActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Animatoo.animateSlideRight(this);
    }

    private void initView() {
        listbankIvBack = findViewById(R.id.listbank_iv_back);
        listbankFabAdd = findViewById(R.id.listbank_fab_add);
        rv = findViewById(R.id.rv);
    }
}
