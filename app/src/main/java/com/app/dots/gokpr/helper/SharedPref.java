package com.app.dots.gokpr.Helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.app.dots.gokpr.model.DataUserModel;
import com.google.gson.Gson;

public class SharedPref {
    public static final String SP = "pinshope";
    public static final String PERTAMA_MASUK = "pertama";
    public static final String SUDAH_LOGIN = "sudah_login";
    public static final String IDUSER = "id";
    public static final String NAMA = "nama";
    public static final String TIPELOGIN = "tipelogin";
    public static final String GMAILLOGIN = "gmailLogin";
    public static final String LISTPROMO = "listpromo";

    SharedPreferences sp;
    SharedPreferences.Editor editor;

    public SharedPref(Context context) {
        sp = context.getSharedPreferences(SP,Context.MODE_PRIVATE);
        editor = sp.edit();
    }

    public void savePrefString(String key, String value){
        editor.putString(key,value);
        editor.commit();
    }

    public void savePrefBoolean(String key, Boolean value){
        editor.putBoolean(key,value);
        editor.commit();
    }


    public void clearAll(){
        editor.clear();
        editor.commit();
    }

    public DataUserModel getData() {
        String data = sp.getString(LISTPROMO, null);
        if (data == null) return null;
        return new Gson().fromJson(data, DataUserModel.class);
    }

    public DataUserModel setData(DataUserModel pesanan) {
        if (pesanan == null) return null;
        String json = new Gson().toJson(pesanan, DataUserModel.class);
        sp.edit().putString(LISTPROMO, json).apply();
        return getData();
    }

    public String getTIPELOGIN() {
        return sp.getString(TIPELOGIN,"");
    }

    public String getGMAILLOGIN() {
        return sp.getString(GMAILLOGIN,"");
    }

    public String getIDUSER() {
        return sp.getString(IDUSER,"");
    }

    public String getNAMA() {
        return sp.getString(NAMA,"");
    }

    public Boolean getSudahLogin(){
        return sp.getBoolean(SUDAH_LOGIN,false);
    }

    public Boolean  getPertamaMasuk() {
        return sp.getBoolean(PERTAMA_MASUK, true);
    }
}
