package com.app.dots.gokpr.Helper;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.app.dots.gokpr.presenter.bank.BankPresenter;
import com.app.dots.gokpr.presenter.profil.ProfilPresenter;
import com.app.dots.gokpr.presenter.promo.PromoPresenterImp;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

public class Loading {

    public static ProgressDialog pd;
    public static SweetAlertDialog asd;
    public static SweetAlertDialog asda;

    public static void onLoading(Context context,String title){
        pd = new ProgressDialog(context);
        pd.setTitle(title);
        pd.setCancelable(false);
        pd.show();
    }
    public static void dialogDismiss(){
        pd.dismiss();
    }

    public static void onFailure(Context context, String pesan) {
        if (context != null){
            SweetAlertDialog dialogKoneksi = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Oops")
                    .setContentText("Periksa Koneksi Anda");
            Log.d("onFilure", " " + pesan);
            if (pesan == "timeout") {
                dialogKoneksi.setContentText("Koneksi Timeout");
                dialogKoneksi.show();
            } else if (pesan.contains("Expected BEGIN_OBJECT")) {
                dialogKoneksi.setContentText("Gagal Mengirim Data");
                dialogKoneksi.show();
            } else {
                dialogKoneksi.show();
            }
        }
    }

    public static void success(Context context, String title, String pesan) {
        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(title)
                .setContentText(pesan)
                .show();
    }

    public static void onLoading(Context context) {
        asd = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        asd.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        asd.setTitleText("Loading");
        asd.setCancelable(false);
        asd.show();
    }

    public static void errorPoin(final Context context, String title, String pesan) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
        pDialog.setTitleText(title)
                .setContentText(pesan)
                .setConfirmText("Reload")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        Loading.dismis();
                        ProfilPresenter p = new ProfilPresenter(context);
                        p.getPoin();
                    }
                });
        pDialog.show();
    }
    public static void errorBank(final Context context, String title, String pesan) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
        pDialog.setTitleText(title)
                .setContentText(pesan)
                .setConfirmText("Reload")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        Loading.dismis();
                        BankPresenter b = new BankPresenter(context);
                        b.getDaftarBank();
                    }
                });
        pDialog.show();
    }
    public static void errorAddBank(final Context context, String title, String pesan, final String id, final String nama, final String rek) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
        pDialog.setTitleText(title)
                .setContentText(pesan)
                .setConfirmText("Reload")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        Loading.dismis();
                        BankPresenter b = new BankPresenter(context);
                        b.insertBank(id,nama,rek);
                    }
                });
        pDialog.show();
    }
    public static void errorRekening(final Context context, String title, String pesan, final RecyclerView recyclerView) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
        pDialog.setTitleText(title)
                .setContentText(pesan)
                .setConfirmText("Reload")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        Loading.dismis();
                        BankPresenter p = new BankPresenter(context);
                        p.getRekening(recyclerView);
                    }
                });
        pDialog.show();
    }

    public static void sendingData(Context context) {
        asd = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        asd.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        asd.setTitleText("Sending Data...");
        asd.setCancelable(false);
        asd.show();
    }

    public static void getingData(Context context) {
        asd = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        asd.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        asd.setTitleText("geting Data...");
        asd.setCancelable(false);
        asd.show();
    }

    public static void onLoadingCustom(Context context, String pesan) {
        asd = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        asd.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        asd.setTitleText(pesan);
        asd.setCancelable(false);
        asd.show();
    }

    public static void basicMessage(Context context, String pesan) {
        new SweetAlertDialog(context)
                .setTitleText(pesan)
                .show();
    }

    public static void waring(final Context context, String title, String pesan) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.setTitleText(title)
                .setContentText(pesan)
                .setConfirmText("Ok");
        pDialog.show();
    }

    public static void reloadPromo(final Context context, String title, String pesan,final ViewPager viewPager) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.setTitleText(title)
                .setContentText(pesan)
                .setConfirmText("Reload")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        PromoPresenterImp promoPresenterImp = new PromoPresenterImp(context);
                        promoPresenterImp.getPromo(viewPager);
                    }
                });
        pDialog.show();
    }

    public static void successWithIntent(final Context context, String title, String pesan, final Class<?> activityTujuan) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.setTitleText(title)
                .setContentText(pesan)
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        context.startActivity(new Intent(context, activityTujuan));
                    }
                });
        pDialog.show();
    }

    public static void successWithIntentString(final Context context, String title, String pesan, final Class<?> activityTujuan, final String extra) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.setTitleText(title)
                .setContentText(pesan)
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        Intent intent = new Intent(context, activityTujuan);
                        intent.putExtra("extra", extra);
                        context.startActivity(intent);
                    }
                });
        pDialog.show();
    }

    public static void dismis() {
        asd.dismiss();
    }
}
