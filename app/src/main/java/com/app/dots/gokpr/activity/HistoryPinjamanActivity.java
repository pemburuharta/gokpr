package com.app.dots.gokpr.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.app.dots.gokpr.fragment.HistoryPinjamanFragment;
import com.app.dots.gokpr.R;

import java.util.ArrayList;
import java.util.List;

public class HistoryPinjamanActivity extends AppCompatActivity {

    private ImageView pinjamanIvBack;
    private TabLayout pinjamanTablayout;
    private ViewPager pinjamanViewpager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_pinjaman);
        getSupportActionBar().hide();
        initView();

        setupViewPager(pinjamanViewpager);
        pinjamanTablayout.setupWithViewPager(pinjamanViewpager);

    }

    private void setupViewPager(ViewPager pinjamanViewpager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new HistoryPinjamanFragment(),"Menunggu");
        adapter.addFragment(new HistoryPinjamanFragment(),"Diproses");
        adapter.addFragment(new HistoryPinjamanFragment(),"Ditolak");
        adapter.addFragment(new HistoryPinjamanFragment(),"Di ACC");
        pinjamanViewpager.setAdapter(adapter);
    }

    private void initView() {
        pinjamanIvBack = findViewById(R.id.pinjaman_iv_back);
        pinjamanTablayout = findViewById(R.id.pinjaman_tablayout);
        pinjamanViewpager = findViewById(R.id.pinjaman_viewpager);
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
        }

        @Override
        public Fragment getItem(int i) {
            return mFragmentList.get(i);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String s) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(s);
        }

    }

}
