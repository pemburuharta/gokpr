package com.app.dots.gokpr.presenter.login;

import android.widget.EditText;

public interface LoginPresenter {
    void Login(EditText edtUsername,EditText edtPassword);
    void Logout();
}
