package com.app.dots.gokpr.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.app.dots.gokpr.R;
import com.app.dots.gokpr.model.DataUserModel;
import com.app.dots.gokpr.presenter.bank.BankPresenter;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;

import java.util.ArrayList;

public class AddBankActivity extends AppCompatActivity {

    private ImageView addbankIvBack;
    private Spinner addbankSpinner;
    private TextView addbankTvNamabank;
    private EditText addbankEdtNama;
    private EditText addbankEdtRekening;
    private CardView addbankCvSave;
    private BankPresenter bankPresenter;
    private String idBank;
    private DataUserModel dataUserModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_bank);
        getSupportActionBar().hide();
        Animatoo.animateSlideUp(this);
        initView();

        start();
    }

    private void start(){
        bankPresenter = new BankPresenter(AddBankActivity.this);
        /*** Set Statusbar Jadi Hitam ***/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = getWindow().getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        /*** Tutup Statusbar Jadi Hitam ***/
        bankPresenter.getDaftarBank();

        mainButton();
    }

    private void mainButton(){
        addbankIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        addbankCvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (! validasi()){
                    return;
                }
                bankPresenter.insertBank(idBank,addbankEdtNama.getText().toString(),addbankEdtRekening.getText().toString());
            }
        });
    }

    public void setBank(ArrayList<DataUserModel> dataUserModel){
        ArrayList<String> listNama = new ArrayList<>();
        final ArrayList<String> listId = new ArrayList<>();

        for (int i = 0; i <dataUserModel.size() ; i++) {
            listNama.add(dataUserModel.get(i).nama_bank);
            listId.add(dataUserModel.get(i).id_bank);
        }

        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(AddBankActivity.this, R.layout.support_simple_spinner_dropdown_item, listNama);
        addbankSpinner.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
        addbankSpinner.setAdapter(adp2);

        addbankSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int idKat = addbankSpinner.getSelectedItemPosition();
                idBank = listId.get(idKat);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void clear(){
        addbankEdtRekening.setText("");
        addbankEdtNama.setText("");

        Intent intent = new Intent(AddBankActivity.this, ListBankActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private boolean validasi() {
        if (addbankEdtNama.getText().toString().isEmpty()) {
            addbankEdtNama.setError("Tidak boleh kosong");
            addbankEdtNama.requestFocus();
            return false;
        }

        if (addbankEdtRekening.getText().toString().isEmpty()) {
            addbankEdtRekening.setError("Tidak boleh kosong");
            addbankEdtRekening.requestFocus();
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Animatoo.animateSlideRight(this);
    }

    private void initView() {
        addbankIvBack = findViewById(R.id.addbank_iv_back);
        addbankSpinner = findViewById(R.id.addbank_spinner);
        addbankTvNamabank = findViewById(R.id.addbank_tv_namabank);
        addbankEdtNama = findViewById(R.id.addbank_edt_nama);
        addbankEdtRekening = findViewById(R.id.addbank_edt_rekening);
        addbankCvSave = findViewById(R.id.addbank_cv_save);
    }
}
