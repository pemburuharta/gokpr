package ai.agusibrhim.design.topedc.Helper;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class HelperClass {

    public HelperClass() {
    }

    public String convertRupiah(Integer x){
        Locale localeID = new Locale("in", "ID");
        NumberFormat format = NumberFormat.getCurrencyInstance(localeID);
        String hasilConvert = format.format((double) x);

        return hasilConvert;
    }

    public String convertDateFormat(String date){
        String hasil="";

        final String formatLama = "yyyy-MM-dd";
        final String formatBaru = "dd-MM-yyyy";

        SimpleDateFormat sdf = new SimpleDateFormat(formatLama);
        try {
            Date dd = sdf.parse(date);
            sdf.applyPattern(formatBaru);
            hasil = sdf.format(dd);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return hasil;
    }

    public String splitText(String string){
        String gb = string.replace("|", " ");
        String strArray[] = gb.split(" ");

        return strArray[0];
    }

    public void dialogError(Context context, String pesan){
        new AlertDialog.Builder(context)
                .setMessage("" + pesan)
                .setCancelable(false)
                .setPositiveButton("Ulangi", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                })
                .show();
    }
}
