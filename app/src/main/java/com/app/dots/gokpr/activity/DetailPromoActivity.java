package com.app.dots.gokpr.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.dots.gokpr.Helper.Config;
import com.app.dots.gokpr.Helper.SharedPref;
import com.app.dots.gokpr.R;
import com.app.dots.gokpr.model.DataUserModel;
import com.squareup.picasso.Picasso;

public class DetailPromoActivity extends AppCompatActivity {

    private ImageView imagePromo;
    private ImageView imgBack;
    private TextView tvJudul;
    private TextView tvTanggal;
    private TextView tvKet;
    private com.app.dots.gokpr.Helper.SharedPref pref;
    private DataUserModel data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_promo);
        getSupportActionBar().hide();
        initView();
        setData();
    }

    private void setData(){
        pref = new SharedPref(this);
        data = pref.getData();

        tvJudul.setText(data.judul);
        tvKet.setText(data.isi);
        tvTanggal.setText("Dengan nilai diskon "+ data.diskon);
        Picasso.with(DetailPromoActivity.this)
                .load(Config.URLIMAGE2+data.gambar)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.no_image_found)
                .into(imagePromo);

        mainButton();
    }

    private void mainButton(){
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initView() {
        imagePromo = findViewById(R.id.image_promo);
        imgBack = findViewById(R.id.img_back);
        tvJudul = findViewById(R.id.tv_judul);
        tvTanggal = findViewById(R.id.tv_tanggal);
        tvKet = findViewById(R.id.tv_ket);
    }
}
