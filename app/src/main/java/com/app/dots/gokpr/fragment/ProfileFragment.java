package com.app.dots.gokpr.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.dots.gokpr.R;
import com.app.dots.gokpr.activity.LoginActivity;
import com.app.dots.gokpr.activity.MainHomeActivity;
import com.app.dots.gokpr.activity.ProfilActivity;
import com.app.dots.gokpr.presenter.login.LoginPresenterImp;
import com.app.dots.gokpr.presenter.profil.ProfilPresenter;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener {


    private TextView tvUsername;
    private TextView tvReveral;
    private TextView tvNama;
    private TextView tvEmail;
    private TextView tvNoTlp;
    private TextView tvKeluar;
    private LoginPresenterImp loginPresenterImp;
    private ProfilPresenter profilPresenter;
    private com.app.dots.gokpr.Helper.SharedPref pref;
    private GoogleApiClient googleApiClient;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        initView(view);
        start();

        return view;
    }

    private void start() {
        pref = new com.app.dots.gokpr.Helper.SharedPref(getActivity());
        loginPresenterImp = new LoginPresenterImp(getActivity());
        profilPresenter = new ProfilPresenter(getActivity());

        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(getActivity()).enableAutoManage(getActivity(), this).addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions).build();

        profilPresenter.getProfil(tvUsername,tvReveral,tvNama,tvEmail,tvNoTlp);

        mainButton();
    }

    private void mainButton() {
        tvKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pref.getTIPELOGIN().equals(com.app.dots.gokpr.Helper.Config.GMAIL)) {
                    Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(@io.reactivex.annotations.NonNull Status status) {
                            pref.clearAll();
                            pref.savePrefBoolean(com.app.dots.gokpr.Helper.SharedPref.SUDAH_LOGIN, false);

                            startActivity(new Intent(getActivity(), LoginActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                            ((MainHomeActivity)getActivity()).finish();
                        }
                    });

                } else {
                    loginPresenterImp.Logout();
                }

            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        googleApiClient.stopAutoManage(getActivity());
        googleApiClient.disconnect();
    }

    private void initView(View view) {
        tvUsername = view.findViewById(R.id.tv_username);
        tvReveral = view.findViewById(R.id.tv_reveral);
        tvNama = view.findViewById(R.id.tv_nama);
        tvEmail = view.findViewById(R.id.tv_email);
        tvNoTlp = view.findViewById(R.id.tv_no_tlp);
        tvKeluar = view.findViewById(R.id.tv_keluar);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
