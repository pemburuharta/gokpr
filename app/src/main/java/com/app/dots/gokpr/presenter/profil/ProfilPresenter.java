package com.app.dots.gokpr.presenter.profil;

import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;

import com.app.dots.gokpr.Helper.SharedPref;
import com.app.dots.gokpr.activity.KeuntunganActivity;
import com.app.dots.gokpr.activity.ProfilActivity;
import com.app.dots.gokpr.model.DataUserModel;
import com.app.dots.gokpr.model.ResponseModel;
import com.app.dots.gokpr.retrofit.ApiConfig;
import com.app.dots.gokpr.retrofit.ApiService;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ProfilPresenter {
    private Context context;
    private com.app.dots.gokpr.Helper.SharedPref pref;
    private DataUserModel dataUserModel;

    public ProfilPresenter(Context context) {
        this.context = context;
        pref = new SharedPref(context);
    }

    public void getProfil(final TextView username, final TextView reveral, final TextView nama, final TextView email, final TextView tlp){
        com.app.dots.gokpr.Helper.Loading.onLoading(context);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getProfil(pref.getIDUSER())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseModel responseModel) {
                        if (responseModel.error == false){
                            dataUserModel = responseModel.data;
                        }else {
                            Toast.makeText(context, ""+responseModel.message, Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        com.app.dots.gokpr.Helper.Loading.dismis();
                        Toast.makeText(context, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {
                        com.app.dots.gokpr.Helper.Loading.dismis();
                        username.setText(dataUserModel.username);
                        reveral.setText("Reveral : "+ dataUserModel.referal);
                        nama.setText(dataUserModel.nama_user);
                        email.setText(dataUserModel.email);
                        tlp.setText(dataUserModel.telp);
                    }
                });
    }

    public void getPoin(){
        com.app.dots.gokpr.Helper.Loading.onLoading(context);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getPoint(pref.getIDUSER())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseModel responseModel) {
                        if (responseModel.error == false){
                            dataUserModel = responseModel.data;
                        }else {
                            Toast.makeText(context, ""+responseModel.message, Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        com.app.dots.gokpr.Helper.Loading.dismis();
                        com.app.dots.gokpr.Helper.Loading.errorPoin(context,"Error",""+e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        com.app.dots.gokpr.Helper.Loading.dismis();
                        ((KeuntunganActivity)context).setPoin(dataUserModel.poin);
                    }
                });
    }
}
