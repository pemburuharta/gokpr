package com.app.dots.gokpr;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.dots.gokpr.activity.HistoryPenarikanActivity;
import com.app.dots.gokpr.activity.HistoryPinjamanActivity;
import com.app.dots.gokpr.activity.KeuntunganActivity;
import com.app.dots.gokpr.activity.MasukanPinjamanActivity;
import com.app.dots.gokpr.activity.ProfilActivity;
import com.app.dots.gokpr.adapter.AdapterPromo;
import com.app.dots.gokpr.model.ModelPromo;
import com.app.dots.gokpr.presenter.promo.PromoPresenter;
import com.app.dots.gokpr.presenter.promo.PromoPresenterImp;
import com.ethanhua.skeleton.Skeleton;
import com.ethanhua.skeleton.SkeletonScreen;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {

    private TextView homeTvUcapan;
    private TextView homeTvUsername;
    private CircleImageView homeIvProfil;
    private ViewPager homeViewPager;
    private CardView homeCvMasukanpinjaman;
    private CardView homeCvKeuntungan;
    private CardView homeCvHistorypinjaman;
    private CardView homeCvHistorypenarikan;
    private LinearLayout lyUtama;
    private PromoPresenterImp promoPresenterImp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        initView();

        start();
    }

    private void start(){
        promoPresenterImp = new PromoPresenterImp(MainActivity.this);
        promoPresenterImp.getPromo(homeViewPager);
        promoPresenterImp.setWaktu(homeTvUcapan,homeTvUsername);

        mainButton();
    }

    private void mainButton() {

        homeCvMasukanpinjaman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MasukanPinjamanActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        homeIvProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ProfilActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        homeCvHistorypenarikan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), HistoryPenarikanActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        homeCvHistorypinjaman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), HistoryPinjamanActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        homeCvKeuntungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), KeuntunganActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

    }


    private void initView() {
        homeTvUcapan = findViewById(R.id.home_tv_ucapan);
        homeTvUsername = findViewById(R.id.home_tv_username);
        homeIvProfil = findViewById(R.id.home_iv_profil);
        homeViewPager = findViewById(R.id.home_viewPager);
        homeCvMasukanpinjaman = findViewById(R.id.home_cv_masukanpinjaman);
        homeCvKeuntungan = findViewById(R.id.home_cv_keuntungan);
        homeCvHistorypinjaman = findViewById(R.id.home_cv_historypinjaman);
        homeCvHistorypenarikan = findViewById(R.id.home_cv_historypenarikan);
        lyUtama = (LinearLayout) findViewById(R.id.ly_utama);
    }
}
