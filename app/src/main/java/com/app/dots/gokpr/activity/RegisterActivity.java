package com.app.dots.gokpr.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.app.dots.gokpr.Helper.SharedPref;
import com.app.dots.gokpr.R;
import com.app.dots.gokpr.presenter.register.RegisterPresenterImp;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;

public class RegisterActivity extends AppCompatActivity {

    private EditText edtUsername;
    private EditText edtNama;
    private EditText edtEmail;
    private EditText edtNo;
    private EditText edtPass;
    private EditText edtReferal;
    private CardView cardDaftar;
    private TextView tvMasuk;
    private RegisterPresenterImp registerPresenterImp;
    private com.app.dots.gokpr.Helper.SharedPref pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().hide();
        Animatoo.animateSlideDown(this);
        initView();

        start();
    }

    private void start() {
        pref = new SharedPref(this);
        registerPresenterImp = new RegisterPresenterImp(RegisterActivity.this);

        if (pref.getGMAILLOGIN().equals("")){
            edtEmail.setText("");
        }else {
            edtEmail.setText(pref.getGMAILLOGIN());
        }
        mainButton();
    }

    private void mainButton() {
        cardDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (! validasi()){
                 return;
                }
                registerPresenterImp.register(edtNama, edtUsername, edtEmail, edtNo, edtPass, edtReferal);
            }
        });

        tvMasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private boolean validasi() {
        if (edtUsername.getText().toString().contains(" ")) {
            edtUsername.setError("Tidak boleh ada spasi");
            edtUsername.requestFocus();
            return false;
        }
        if (!edtEmail.getText().toString().contains("@")){
            edtEmail.setError("Email Tidak Valid");
            edtEmail.requestFocus();
            return false;
        }
        if (edtNo.getText().toString().length() < 11){
            edtNo.setError("Nomor tidak valid");
            edtNo.requestFocus();
        }
        if (edtPass.getText().toString().length() < 6){
            edtPass.setError("Password minimal 6 karakter");
            edtPass.requestFocus();
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Animatoo.animateSlideRight(this);
    }

    private void initView() {
        edtUsername = findViewById(R.id.edt_username);
        edtNama = findViewById(R.id.edt_nama);
        edtEmail = findViewById(R.id.edt_email);
        edtNo = findViewById(R.id.edt_no);
        edtPass = findViewById(R.id.edt_pass);
        edtReferal = findViewById(R.id.edt_referal);
        cardDaftar = findViewById(R.id.card_daftar);
        tvMasuk = findViewById(R.id.tv_masuk);
    }
}
