package com.app.dots.gokpr.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.dots.gokpr.Helper.Config;
import com.app.dots.gokpr.R;
import com.app.dots.gokpr.model.DataUserModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdapterDaftarBank extends
        RecyclerView.Adapter<AdapterDaftarBank.ViewHolder> {

    private static final String TAG = AdapterDaftarBank.class.getSimpleName();

    private Context context;
    private ArrayList<DataUserModel> list;


    public AdapterDaftarBank(Context context, ArrayList<DataUserModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_rekening,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        DataUserModel item = list.get(position);
        holder.tvNoRek.setText(item.rekening);
        holder.tvNamaBank.setText(item.nama_bank +" - "+item.nama);
        Picasso.with(context)
                .load(Config.URLIMAGE2+item.gambar)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.no_image_found)
                .into(holder.imgBank);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void clear() {
        int size = this.list.size();
        this.list.clear();
        notifyItemRangeRemoved(0, size);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgBank;
        private TextView tvNamaBank;
        private TextView tvNoRek;

        public ViewHolder(View itemView) {
            super(itemView);
            imgBank = itemView.findViewById(R.id.img_bank);
            tvNamaBank = itemView.findViewById(R.id.tv_nama_bank);
            tvNoRek = itemView.findViewById(R.id.tv_no_rek);
        }
    }
}